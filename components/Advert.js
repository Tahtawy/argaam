import React, { Component } from "react";

export default class Ad extends Component {
  componentDidMount() {
    googletag.cmd.push(() => {
      googletag.defineSlot(this.props.adUrl, [300, 250], 'div-gpt-ad-1553172904941-0').addService(googletag.pubads());
      googletag.pubads().enableSingleRequest();
      googletag.pubads().collapseEmptyDivs();
      googletag.enableServices();
    });
    googletag.cmd.push(function () { googletag.display('div-gpt-ad-1553172904941-0'); });
  }
  render() {
    return <div id='div-gpt-ad-1553172904941-0'></div>;
  }
}
