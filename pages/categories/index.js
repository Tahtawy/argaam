import React from 'react'
import axiosInstance from '../../axiosInstance'
import Link from 'next/link'
import { withRouter } from 'next/router'
import Head from '../../components/head'
import Router from 'next-server/dist/server/router';
import NextRouter from 'next/router'

class Home extends React.Component {
  static async getInitialProps({ query }) {
    const response = await axiosInstance.get('/sources?apiKey=a1560079905d4c2e8a478cc484d1b11f');
    return {
      categories: response.data.sources.filter((source) => {
        return source.category === query.category;
      })
    };
  }

  render() {
    return (
      <div>
        <Head title={this.props.router.query.category} />

        <div className="hero">
          <h1 className="title">Welcome to {this.props.router.query.category} page!</h1>
          <p className="description">
            Which website you need to visit ?
          </p>
          <div className="go-back">
            <button onClick={() => NextRouter.back()}>Go Back</button>
          </div>

          <div className="row">
            {
              this.props.categories.map(category => {
                return (
                  <Link key={category.id} href={'/categories/sources?source=' + category.id}
                    as={'/categories/sources/' + category.id}>
                    <a className="card">
                      <h3>{category.name} &rarr;</h3>
                      <p>{category.description}</p>
                    </a>
                  </Link>
                )
              })
            }
          </div>
        </div>

        <style jsx>{`
          .hero {
            width: 100%;
            color: #333;
          }
          .title {
            margin: 0;
            width: 100%;
            padding-top: 80px;
            line-height: 1.15;
            font-size: 48px;
          }
          .title,
          .description {
            text-align: center;
          }
          .row {
            max-width: 880px;
            margin: 80px auto 40px;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: space-around;
          }
          .card {
            padding: 18px 18px 24px;
            width: 220px;
            text-align: left;
            text-decoration: none;
            color: #434343;
            border: 1px solid #9b9b9b;
            margin-bottom: 15px;
          }
          .card:hover {
            border-color: #067df7;
          }
          .card h3 {
            margin: 0;
            color: #067df7;
            font-size: 18px;
          }
          .card p {
            margin: 0;
            padding: 12px 0 0;
            font-size: 13px;
            color: #333;
          }
          .go-back {
            text-align: center;
          }
          .go-back button {
            cursor: pointer;
          }
        `}</style>
      </div>
    );
  }
}

export default withRouter(Home)
