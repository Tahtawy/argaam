import React from 'react'
import Link from 'next/link'
import Head from '../components/head'
import Ad from '../components/Advert'

class Home extends React.Component {
  render() {
    return (
      <div>
        <Head title="Home" />

        <div className="hero">
          <h1 className="title">Welcome to News Portal!</h1>
          <p className="description">
            To get started, select one news' category from below.
          </p>

          <div className="page-container">
            <Ad className="adStyle" adUrl="/96551095/V2_Masrawy_Desktop/Masrawy_Desktop_Homepage/Masrawy_Desktop_Homepage_SC1" />
            <script src="../static/pushAd.js"></script>
            <section className="page-content">
              <div className="row">
                <Link href="/categories?category=general" as="/categories/general">
                  <a className="card">
                    <h3>General &rarr;</h3>
                    <p>Know latest news from all types.</p>
                  </a>
                </Link>
                <Link href="/categories?category=sports" as="/categories/sports">
                  <a className="card">
                    <h3>Sports &rarr;</h3>
                    <p>Know about latest sports' news.</p>
                  </a>
                </Link>
                <Link href="/categories?category=business" as="/categories/business">
                  <a className="card">
                    <h3>Business &rarr;</h3>
                    <p>K now about what's new in business world.</p>
                  </a>
                </Link>
              </div>
            </section>
          </div>
        </div>

        <style jsx>{`
          .hero {
            width: 100%;
            color: #333;
          }
          .title {
            margin: 0;
            width: 100%;
            padding-top: 80px;
            line-height: 1.15;
            font-size: 48px;
          }
          .title,
          .description {
            text-align: center;
          }
          .page-container {
            display: flex;
            justify-content: center;
          }
          .page-content {
            margin-left: 20px;
          }
          .row {
            max-width: 880px;
            margin: 80px auto 40px;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: space-around;
          }
          .card {
            padding: 18px 18px 24px;
            width: 220px;
            text-align: left;
            text-decoration: none;
            color: #434343;
            border: 1px solid #9b9b9b;
            margin-bottom: 15px;
          }
          .card:hover {
            border-color: #067df7;
          }
          .card h3 {
            margin: 0;
            color: #067df7;
            font-size: 18px;
          }
          .card p {
            margin: 0;
            padding: 12px 0 0;
            font-size: 13px;
            color: #333;
          }
        `}</style>
      </div>
    );
  }
}

export default Home
