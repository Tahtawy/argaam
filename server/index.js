const express = require('express');
const next = require('next');

const PORT = process.env.PORT || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare().then(() => {
  const server = express();
  const showRoutes = require("./routes/index.js");

  server.use("/api", showRoutes);

  server.get('/categories/:slug', (req, res) => {
    let actualPage = '/categories'
    let queryParams = { category: req.params.slug }
    return app.render(req, res, actualPage, queryParams);
  });

  server.get('/categories/sources/:slug', (req, res) => {
    let actualPage = '/categories/sources'
    let queryParams = { source: req.params.slug }
    return app.render(req, res, actualPage, queryParams);
  });

  server.get('*', (req, res) => {
    return handle(req, res);
  });

  server.listen(PORT, err => {
    if (err) throw err;
    console.log(`> Ready on ${PORT}`);
  });
})
  .catch(ex => {
    console.error(ex.stack);
    process.exit(1);
  })